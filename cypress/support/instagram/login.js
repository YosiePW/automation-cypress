Cypress.Commands.add("loginUser", () => {
    cy.visit(Cypress.config('URLinstagram') + ("login"));
    cy.fixture("instagram/data_user").then((data) => {
      cy.get("#username").type(data.username);
      cy.get("#password").type(data.password);
    });
    cy.get('button[type="submit"]').contains('Login').click()
    cy.url().should('include', Cypress.config('URLinstagram'))
  });