Cypress.Commands.add("loginSuperUser", () => {
    cy.visit(Cypress.config('URLbeasiswa') + ("login"));
    cy.fixture("beasiswa/data_user").then((data) => {
      cy.get("#email").type(data.email);
      cy.get("#password").type(data.password);
    });
    cy.get('button[type="submit"]').contains('Masuk').click()
    cy.url().should('include', Cypress.config('URLbeasiswa'))
});