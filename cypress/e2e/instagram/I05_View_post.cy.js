describe('View post and give it comment', () => {
    it('View post', () => {
        let fixtures = null;
        cy.fixture('instagram/data_post').then(function (data) {
            fixtures = data;
        
        cy.loginUser()
        cy.visit(Cypress.config('URLinstagram') + ('profile'))
        cy.get('.row>.col-md-4').each((element) => {
            if (element.find('div.card-body>p.card-text').text() === fixtures.caption) {
                cy.get(element).find('p').contains('View').click()
            }
        })
        cy.log(fixtures)
        cy.get('#commentBox').type(fixtures.comment).type('{enter}')
        cy.contains(fixtures.comment).should('be.visible')
    })
    });
})