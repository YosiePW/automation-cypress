const url = Cypress.config('URLinstagram');
describe('Add Post (positif case)', () => {
    let fixtures = null;

    beforeEach(() => {
        cy.loginUser()
        cy.visit(url + ('post/create'))
        cy.fixture('instagram/data_post').then(function (data) {
            fixtures = data;
        })
    })
    it('add post', () => {
        const filepath = 'images/gambar-jpg.jpg'
        cy.get('#customFile').attachFile(filepath)
        cy.get('#caption').type(fixtures.caption)
        cy.get('.btn').contains('Post').click()
        cy.contains(fixtures.caption).should('be.visible')

    });

    it('add post again', () => {
        const filepath = 'images/gambar-png.png'
        cy.get('#customFile').attachFile(filepath)
        cy.get('#caption').type(fixtures.caption2)
        cy.get('.btn').contains('Post').click()
        cy.contains(fixtures.caption2).should('be.visible')
    });
})

describe('Add post (negatif case)', () => {
    beforeEach(() => {
        cy.loginUser()
        cy.visit(url + ('post/create'))
    })

    it('do not fill in the caption fields', () => {
        const filepath = 'images/gambar-png.png'
        cy.get('#customFile').attachFile(filepath)
        cy.get('.btn').contains('Post').click()
        cy.get('.invalid-feedback').contains('The Caption field is required.').should('be.visible')
    });

    it('do not fill in the photo fields', () => {
        cy.get('#caption').type('haloo')
        cy.get('.btn').contains('Post').click()
        cy.get('.invalid-feedback').contains('You did not select a file to upload.').should('be.visible')
    });
})
