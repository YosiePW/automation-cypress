const url = Cypress.config('URLinstagram');
let fixtures = null;

describe('View Post (positif case)', () => {
    
    beforeEach(() => {
        cy.loginUser()
        cy.visit(url + ('profile'))
        cy.fixture('instagram/data_post').then(function (data) {
            fixtures = data;
        })
        
    })

    it('edit post', () => {
        cy.get('.row>.col-md-4').each((element) => {
            if (element.find('div.card-body>p.card-text').text() === fixtures.caption) {
                cy.get(element).find('p').contains('Edit').click()
            }
        })
        cy.get('#caption').clear().type(fixtures.caption_edit)
        cy.get('.btn-primary').click()
        cy.contains(fixtures.caption_edit).should('be.visible')
    });
})

describe('Add post (negatif case)', () => {
    beforeEach(() => {
        cy.loginUser()
        cy.visit(url + ('profile'))
        cy.fixture('instagram/data_post').then(function (data) {
            fixtures = data;
        })
    })

    it('Tidak mengisi kolom caption', () => {
        cy.get('.row>.col-md-4').each((element) => {
            if (element.find('div.card-body>p.card-text').text() === fixtures.caption_edit) {
                cy.get(element).find('p').contains('Edit').click()
            }
        })
        cy.get('#caption').clear()
        cy.get('.btn').contains('Post').click()
        cy.get('.invalid-feedback').contains('The Caption field is required.').should('be.visible')
    });

})
