describe('Login (positif case)', () => {
    it('Do login', () => {
        cy.loginUser()
    });
});

describe('Login (negatif case)', () => {
    beforeEach(() => {
        cy.visit(Cypress.config('URLinstagram') + ("login"));
    })

    it('Login with invalid username ', () => {
        cy.fixture("instagram/data_user").then((data) => {
            cy.get("#username").type('haloo');
            cy.get("#password").type(data.password);
        });
        cy.get('button[type="submit"]').contains('Login').click()
        cy.get('.alert-danger').contains('Username atau Password salah!').should('be.visible')
    });

    it('Login with invalid password ', () => {
        cy.fixture("instagram/data_user").then((data) => {
            cy.get("#username").type(data.username);
            cy.get("#password").type('haloo');
        });
        cy.get('button[type="submit"]').contains('Login').click()
        cy.get('.alert-danger').contains('Username atau Password salah!').should('be.visible')
    });
});