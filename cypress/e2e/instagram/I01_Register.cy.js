describe('Register', () => {
            it('Do Register', () => {
                cy.exec("dropdb -U postgres insta_test");
                cy.exec("createdb -U postgres -T insta_apps insta_test");
                cy.visit(Cypress.config('URLinstagram') + ("register"));
                cy.fixture("instagram/data_user").then((data) => {
                    cy.get('#nama').type(data.nama)
                    cy.get('#username').type(data.username)
                    cy.get('#password').type(data.password)
                    cy.get('button[type="submit"]').contains('Register').click()
                    cy.url().should('include', Cypress.config('URLinstagram'));
                    cy.contains(data.username).should('be.visible')
                });
            });

                it('Do Register Negatif Case(username sudah digunakan)', () => {
                    cy.visit(Cypress.config('URLinstagram') + ("register"));
                    cy.fixture("instagram/data_user").then((data) => {
                        cy.get('#nama').type(data.nama)
                        cy.get('#username').type(data.username)
                        cy.get('#password').type(data.password)
                        cy.get('button[type="submit"]').contains('Register').click()
                        cy.get('.alert-danger').contains('Username tersebut sudah digunakan!').should('be.visible')
                    });
                });

            });