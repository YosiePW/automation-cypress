describe('Register (valid)', () => {
    it('Do register', () => {
        cy.exec("dropdb -U postgres beasiswa_test");
        cy.exec("createdb -U postgres -T semesta_app beasiswa_test");
        cy.visit(Cypress.config('URLbeasiswa') + ("register"));
        cy.fixture("beasiswa/data_user").then((data) => {
            cy.get('#email').type(data.email)
            cy.get('#password').type(data.password)
            cy.get('button').contains('Daftar').click()
            cy.contains('Verifikasi Email Kamu').should('be.visible')
        });
    });
})

describe('Register (invalid)', () => {
    beforeEach(() =>{
        cy.visit(Cypress.config('URLbeasiswa') + ("register"));
    })
    it('Password kurang dari 6 karakter', () => {
        cy.fixture("beasiswa/data_user").then((data) => {
            cy.get('#email').type(data.email)
            cy.get('#password').type('123')
            cy.get('button').contains('Daftar').click()
            cy.get('.invalid-feedback').contains('Password harus minimal 6 karakter.').should('be.visible')
        });
    });

    it('Tidak mengandung huruf kapital', () => {
        cy.fixture("beasiswa/data_user").then((data) => {
            cy.get('#email').type(data.email)
            cy.get('#password').type('123123')
            cy.get('button').contains('Daftar').click()
            cy.get('.invalid-feedback').contains('Password harus mengandung setidaknya satu huruf besar dan satu huruf kecil.').should('be.visible')
        });
    });

    it('Tidak mengandung karakter', () => {
        cy.fixture("beasiswa/data_user").then((data) => {
            cy.get('#email').type(data.email)
            cy.get('#password').type('C1ka123')
            cy.get('button').contains('Daftar').click()
            cy.get('.invalid-feedback').contains('Password harus mengandung setidaknya satu simbol.').should('be.visible')
        });
    });

    it('Email sudah digunakan', () => {
        cy.fixture("beasiswa/data_user").then((data) => {
            cy.get('#email').type(data.email)
            cy.get('#password').type('C1ka123@')
            cy.get('button').contains('Daftar').click()
            cy.get('.invalid-feedback').contains('Email tersebut sudah digunakan.').should('be.visible')
        });
    });
})