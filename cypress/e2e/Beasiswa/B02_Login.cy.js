describe('Login (valid)', () => {
    it('Do login', () => {
        cy.loginSuperUser()
    });
})

describe('Login (invalid)', () => {
    let fixtures = null
    beforeEach(() => {
        cy.visit(Cypress.config('URLbeasiswa') + ("login"));
        cy.fixture("beasiswa/data_user").then((data) => {
            fixtures = data
        });
    })
    it('Login username salah', () => {
        cy.get("#email").type('aloo@gmail.com');
        cy.get("#password").type(fixtures.password);
        cy.get('button[type="submit"]').contains('Masuk').click()
        cy.contains('Email atau Password kamu salah').should('be.visible')
    });

    it('Login password salah', () => {
        cy.get("#email").type(fixtures.email);
        cy.get("#password").type('alooo');
        cy.get('button[type="submit"]').contains('Masuk').click()
        cy.contains('Email atau Password kamu salah').should('be.visible')
    });

    it('Login username dan password salah', () => {
        cy.get("#email").type('aloo@gmail.com');
        cy.get("#password").type('aloo2');
        cy.get('button[type="submit"]').contains('Masuk').click()
        cy.contains('Email atau Password kamu salah').should('be.visible')
    });
})