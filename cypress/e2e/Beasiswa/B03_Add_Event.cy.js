describe('Add event (positif)', () => {
    it('Add event', () => {
        cy.loginSuperUser()
        cy.visit(Cypress.config('URLbeasiswa') + ("events"));
        cy.get('.btn').contains('Tambah Beasiswa').click()
        cy.fixture("beasiswa/data_event").then((data) => {
            const file = 'images/gambar-png.png'
            cy.get('#logo').attachFile(file)
            cy.get('#name').type(data.name);
            cy.get('#description').type(data.deskripsi)
            cy.get('#total_winner').type(data.total_pemenang);
            cy.get('#total_prize').type(data.total_hadiah)
            cy.get('#started_at').type(data.tgl_mulai)
            cy.get('#finished_at').type(data.tgl_selesai)
            cy.get('#registration_period_started_at').type(data.tgl_reg)
            cy.get('#registration_period_finished_at').type(data.tgl_selesai_reg)
            cy.get('#is_active').check();
            cy.get('button').contains('Simpan').click();
            cy.contains('Beasiswa berhasil ditambahkan').should('be.visible');
        });
    });
})

describe('Add event (negatif)', ()=> {
    it('Menambahkan event dgn tgl registrasi lebih awal dari tgl mulai', () => {
        
    });

    it('Menambahkan event dgn tgl selesai lebih awal dari tgl mulai', () => {
        
    });

    it('Tidak mengisi kolom mandatory', () => {
        
    });
})