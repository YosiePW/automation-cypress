describe('View Detil', ()=> {
    it('edit event', () => {
        cy.loginSuperUser()
        cy.visit(Cypress.config('URLbeasiswa') + ("events"));
        cy.fixture("beasiswa/data_event").then((data) => {
            cy.get('.table>tbody>tr').each((el) => {
                if (el.find('td:nth-child(2)').text() === data.name) {
                    cy.get(el).find('a[aria-label="Lihat Beasiswa"]').invoke('removeAttr', 'target').click()
                    cy.contains(data.name).should('be.visible')
                }
            })
        })
    });
})