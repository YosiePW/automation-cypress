describe('Add jenjang karir (positif)', () => {
    it('Melakukan tambah jenjang karir', () => {
        cy.loginSuperUser()
        cy.visit(Cypress.config('URLbeasiswa') + ("master/career-paths"));
        cy.get('.card-header-elements > .btn').click();
        cy.get('#name').type('Quality Assurance');
        cy.get('#description').type('Bertugas untuk melakukan testing')
        cy.get('.btn').click();
        cy.get('.swal2-confirm').click();
        /* ==== End Cypress Studio ==== */
    });
})