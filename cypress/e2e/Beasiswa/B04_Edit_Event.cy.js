describe('Edit event (positif)', () => {
    it('edit event', () => {
        cy.loginSuperUser()
        cy.visit(Cypress.config('URLbeasiswa') + ("events"));
        cy.fixture("beasiswa/data_event").then((data) => {
            cy.get('.table>tbody>tr').each((el) => {
                if (el.find('td:nth-child(2)').text() === data.name) {
                    cy.get(el).find('a[aria-label="Ubah Beasiswa"]').click()
                    const file = 'images/gambar-png.png'
                    cy.get('#logo').attachFile(file)
                    cy.get('#description').clear().type(data.edit_deskripsi)
                    cy.get('.btn').contains('Perbarui').click()
                    cy.contains('Beasiswa berhasil diperbarui').should('be.visible')
                }
            })
        })
    });

    it('edit event 2', () => {
        cy.loginSuperUser()
        cy.visit(Cypress.config('URLbeasiswa') + ("events"));
        cy.fixture("beasiswa/data_event").then((data) => {
            cy.get('table').contains(data.name).next().next().next().next().next().find('.btn-warning').click()
        });
    });
})