const { defineConfig } = require("cypress");

module.exports = defineConfig({
  URLinstagram: 'http://localhost/ci-postgram/',
  URLbeasiswa: 'http://127.0.0.1:8000/',
  defaultCommandTimeout: 3000,
  reporter: 'cypress-mochawesome-reporter',
  chromeWebSecurity: false,
  e2e: {
    experimentalStudio: true,
    setupNodeEvents(on, config) {
      require('cypress-mochawesome-reporter/plugin')(on);
    },
  },
});
